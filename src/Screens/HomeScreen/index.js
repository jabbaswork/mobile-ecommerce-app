import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native';
import React from 'react';
import {connect} from 'react-redux';
import { getAllProducts } from '../../Store/Actions';

const HomeScreen = props => {
  return (
    <View style={{flex: 1, backgroundColor: '#DAD3C8'}}>
      <View style={styles.imageContainer}>
        <Image source={require(
// @ts-ignore
        '../../Assets/images/Bitmap.png')} />
      </View>
      <View style={styles.second}>
        <Text style={styles.titleText1}>Elegant</Text>
        <Text style={styles.titleText}>Simple</Text>
        <Text style={styles.titleText}>Furnitures.</Text>
        <Text style={styles.subTitleText}>Affordable home furniture</Text>
        <Text style={styles.subTitleText}> designs & ideas</Text>
      </View>
      <View style={styles.startBtnContainer}>
        <View style={styles.startBtn}>
          <TouchableOpacity
            onPress={() => {
              props.navigation.navigate('Menu');
            }}>
            <Text style={styles.startText}>START</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  imageContainer: {
    flex: 6.5,
    width: 429,
    height: 54,
    borderRadius: 20,
    resizeMode: 'contain',
  },
  second: {
    flex: 2,
    backgroundColor: '#DAD3C8',
    paddingLeft: 27,
  },
  titleText1: {
    bottom: 135,
    paddingLeft: 27,
    position: 'absolute',
    fontFamily: 'Stadt',
    fontStyle: 'normal',
    fontWeight: '400',
    fontSize: 40,
    letterSpacing: 0.04,
    textTransform: 'capitalize',
    color: '#262525',
  },
  titleText: {
    fontFamily: 'Stadt',
    fontStyle: 'normal',
    fontWeight: '400',
    fontSize: 40,
    letterSpacing: 0.04,
    textTransform: 'capitalize',
    color: '#262525',
  },
  subTitleText: {
    fontSize: 18,
    color: ' #645F57',
  },
  startBtnContainer: {
    flex: 1.5,
    display: 'flex',
    flexDirection: 'row',
    alignSelf: 'flex-end',
  },
  startBtn: {
    margin: 5,
    marginRight: 20,
    display: 'flex',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    width: 70,
    height: 70,
    borderRadius: 35,
    backgroundColor: '#62442B',
    boxShadow: '0px 10px 29.7199px rgba(0, 0, 0, 0.3)',
  },
  startText: {
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 27,
    letterSpacing: 0.02,
    textTransform: 'capitalize',
    color: '#CEBB9E',
  },
});

const mapStateToProps = state => {
  return {
    allProducts: state.products.allProducts,
  };
};

const mapDispatchToProps = dispatch => ({
  getAllProducts: () => dispatch(getAllProducts()),
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
