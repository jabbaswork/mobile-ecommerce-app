import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image } from 'react-native'
import React, { useEffect, useState } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import { getCartData } from '../../Store/Actions';
import { connect } from 'react-redux';
import { useIsFocused } from "@react-navigation/native";
import AsyncStorage from '@react-native-async-storage/async-storage';
import CartItem from './item';

const CartScreen = (props) => {
  const { navigation, getCartData, cartItems, totalPrice } = props;
  const isFocused = useIsFocused();

  useEffect(() => {
    // Call only when screen open or when back on screen 
    if (isFocused) {
      getCartData();
    }
  }, [isFocused]);

  const removeItem = async (/** @type {Number} */ id) => {
    try {
      let tempCart = JSON.parse(await AsyncStorage.getItem('CART') || "[]");
      const preExist = tempCart.findIndex((/** @type {{ id: any; }} */ item) => item.id === id)
      tempCart.splice(preExist, 1)
      await AsyncStorage.setItem('CART', JSON.stringify(tempCart))
    } catch (e) {
      console.log("E _________", e)
    }
    getCartData()
  }
  const unitSelect = async (/** @type {any} */ id, /** @type {any} */ count, /** @type {any} */ price) => {
    try {
      let tempCart = JSON.parse(await AsyncStorage.getItem('CART') || "[]");
      const preExist = tempCart.findIndex((/** @type {{ id: any; }} */ item) => item.id === id)
      tempCart[preExist].count = count;
      tempCart[preExist].price = price * tempCart[preExist].count;
      await AsyncStorage.setItem('CART', JSON.stringify(tempCart))
    } catch (e) {
      console.log("E _________", e)
    }
    getCartData()
  }

  return (
    <ScrollView contentContainerStyle={{ flex: 1 }}>
      <View style={styles.outer}>
        <View style={styles.Container}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Main');
            }}>
            <View style={styles.iconContainer}>
              <Icon name="arrow-left" size={20} color="#FFFFFF" />
            </View>
          </TouchableOpacity>
          <View style={styles.titleContainer}>
            <Text style={styles.titleText}>MY CART</Text>
          </View>
        </View>
        {cartItems?.length == 0 ? <View
         style={{
         padding: 50
        }}
        >
          <Image 
          style={{
            width: '100%',
            height: '100%'
          }}
          source={require(
        // @ts-ignore
        '../../Assets/images/nodata.png')} />
          </View>: <Text></Text>} 
        {cartItems?.length ? <ScrollView
          showsVerticalScrollIndicator={false}>
          {cartItems && cartItems?.map((/** @type {{ id: any; }} */ item) => {
            return <CartItem
              key={item.id}
              item={item}
              unitSelect={unitSelect}
              removeItem={removeItem}
            />
          })}
        </ScrollView>: <Text></Text>}
        {cartItems?.length ? <View>
          <View style={styles.totalContainer}>
            <View style={styles.totalContainer1}>
              <Text style={styles.totalText}>{'Subtotal'}</Text>
              <Text style={styles.totalText}>${totalPrice}</Text>
            </View>


            <View style={styles.totalContainer1}>
              <Text style={styles.totalText}>{'Shipping'}</Text>
              <Text style={styles.totalText}>${5}</Text>
            </View>


            <View style={styles.totalContainer1}>
              <Text style={styles.totalText}>{'Total'}</Text>
              <Text style={styles.totalText}>${totalPrice + 5}</Text>
            </View>
          </View>

          <TouchableOpacity>
            <View style={styles.checkOutBtn}>
              <Text style={styles.checkText}>CHECKOUT</Text>
            </View>
          </TouchableOpacity>
        </View> : <Text></Text>}
      </View>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  outer: {
    backgroundColor: '#ff69b4',
    padding: 20,
    display: 'flex',
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'space-between'
  },
  Container: {
    marginTop: 10,
    height: 40,
    display: 'flex',
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center'
  },
  iconContainer: {
    flex: 1
  },
  titleContainer: {
    flex: 1,
    marginRight: 20
  },
  titleText: {
    fontWeight: '900',
    fontSize: 20,
    color: '#FFFFFF',
    alignSelf: 'center',
  },
  recentCard: {
    width: '100%',
    height: 108,
    backgroundColor: '#ffffff',
    borderRadius: 20,
    marginTop: 10,
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 15,
  },
  recentCard2: {
    display: 'flex',
    flexDirection: 'row',
  },
  recentCard3: {
    width: 80,
    height: 80,
    backgroundColor: '#FF1493',
    borderRadius: 20,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    // padding: 30,
    marginRight: 10
  },
  recentCard4: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignContent: 'center',
  },
  title: {
    fontWeight: '700',
    fontSize: 20,
    lineHeight: 27,
    textTransform: 'capitalize',
    color: '#ff69b4',
  },
  price: {
    fontWeight: 'bold',
    fontSize: 18,
    lineHeight: 32,
    letterSpacing: 0.04,
    textTransform: 'capitalize',
    color: '#FF1493',
  },
  inputContainer: {
    display: 'flex',
    flexDirection: 'row',
    width: 80,
    height: 35,
    backgroundColor: '#ff69b4',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderRadius: 15,
    paddingHorizontal: 10,
  },
  plusBtn: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontSize: 20
  },
  totalContainer: {
    width: '100%',
    borderRadius: 20,
    display: 'flex',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    marginVertical: 10
  },
  totalContainer1: {
    width: '100%',
    height: 30,
    backgroundColor: '#FFFFFF',
    borderRadius: 5,
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 15,
    marginTop: 5
  },
  checkOutBtn: {
    width: '100%',
    height: 50,
    backgroundColor: '#FFFFFF',
    borderRadius: 20,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 15,
    marginTop: 5
  },
  totalText: {
    color: '#000000',
    fontSize: 14
  },
  checkText: {
    color: '#000000',
    fontWeight: 'bold',
    fontSize: 20
  }
})

const mapStateToProps = (state) => {
  return {
    cartItems: state.products.cartItems,
    totalPrice: state.products.totalPrice,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getCartData: () => dispatch(getCartData()),
});

export default connect(mapStateToProps, mapDispatchToProps)(CartScreen);