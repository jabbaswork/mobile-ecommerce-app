import { View, Text, StyleSheet, Image, TouchableOpacity,  } from 'react-native'
import React, { useEffect, useState } from 'react'
import { truncate } from '../../Utils/functions';
import Icon from 'react-native-vector-icons/FontAwesome';


const CartItem = (props)=> {
 const {item, unitSelect, removeItem} = props;
 const [count, setCount] = useState(1);
   
 useEffect(() => {
     unitSelect(item?.id, count, item.refPrice)
  }, [count])
  
  useEffect(() => {
    setCount(item.count)
  }, [item.count])
 
 return (<View key={item.id} style={styles.recentCard}>
    <View style={styles.recentCard2}>
      <View style={styles.recentCard3}>
        <Image
         style={{
          borderRadius: 20,
          width: '100%',
          height: '100%',
          resizeMode: 'center'
        }}
         source={{uri: item?.image}}
        />
      </View>
      <View style={{ width: '75%', paddingRight: 10 }}>
        <View style={styles.recentCard4}>
          <View>
            <Text style={styles.title}> {truncate(item.title, 20)}</Text>

            <Text>{item.category}</Text>
          </View>
          <TouchableOpacity
              onPress={() =>removeItem(item.id)}>
          <Icon name="trash-o" size={25} color="#FF1493" />
          </TouchableOpacity>
        </View>
        <View style={styles.recentCard4}>
          <Text style={styles.price}>${item.price}</Text>
          <View style={styles.inputContainer}>
            <TouchableOpacity
              onPress={() => { 
                if (count > 1) {
                  setCount(count - 1)}}}>

              <Text style={styles.plusBtn}>-</Text>
            </TouchableOpacity>
            <Text style={styles.plusBtn}>{item.count}</Text>
            <TouchableOpacity
             onPress={() => { 
              if (count <  20) {
                setCount(count + 1)}
             }}
            >
              <Text style={styles.plusBtn}>+</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>

  </View>
  )
}


const styles = StyleSheet.create({
    Container: {
      marginTop: 10,
      display: 'flex',
      flexDirection: 'row',
      alignContent: 'center',
      alignItems: 'center'
    },
    iconContainer: {
      flex: 1
    },
    titleContainer: {
      flex: 1,
      marginRight: 20
    },
    titleText: {
      fontWeight: '900',
      fontSize: 20,
      color: '#FFFFFF',
      alignSelf: 'center',
    },
    recentCard: {
      width: '100%',
      height: 108,
      backgroundColor: '#ffffff',
      borderRadius: 20,
      marginTop: 10,
      display: 'flex',
      justifyContent: 'space-between',
      flexDirection: 'row',
      alignContent: 'center',
      alignItems: 'center',
      paddingHorizontal: 15,
    },
    recentCard2: {
      display: 'flex',
      flexDirection: 'row',
    },
    recentCard3: {
      width: 80,
      height: 80,
      backgroundColor: 'FF1493',
      borderRadius: 20,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      alignContent: 'center',
      // padding: 30,
      marginRight: 10
    },
    recentCard4: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignContent: 'center',
    },
    title: {
      fontWeight: '700',
      fontSize: 20,
      lineHeight: 27,
      textTransform: 'capitalize',
      color: '#ff69b4',
    },
    price: {
      fontWeight: 'bold',
      fontSize: 18,
      lineHeight: 32,
      letterSpacing: 0.04,
      textTransform: 'capitalize',
      color: '#FF1493',
    },
    inputContainer: {
      display: 'flex',
      flexDirection: 'row',
      width: 80,
      height: 35,
      backgroundColor: '#ff69b4',
      alignContent: 'center',
      alignItems: 'center',
      justifyContent: 'space-between',
      borderRadius: 15,
      paddingHorizontal: 10,
    },
    plusBtn: {
      color: '#FFFFFF',
      fontWeight: 'bold',
      fontSize: 20
    }
  })

  export default CartItem;