
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Image,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import ContentLoader, { Rect, Circle } from 'react-content-loader/native'
import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';
import { fetchSingleProductLoading, getCartData, getSingleProduct } from '../../../Store/Actions';
import { truncate } from '../../../Utils/functions';

const SingleProduct = props => {
  const { route, singleProduct, getSingleProduct, getCartData, cartItems, fetchSingleProductLoading, singleProductLoading } = props;
  const id = route?.params?.id;

  useEffect(() => {
    console.log("WE ARE HERE")
    getSingleProduct(id);
    getCartData();
  }, []);

  useEffect(() => {
    if (singleProduct) fetchSingleProductLoading(false)
  }, [singleProduct]);


  const saveData = async (product) => {
    try {
      let tempCart = JSON.parse(await AsyncStorage.getItem('CART') || "[]");
      const preExist = tempCart.findIndex(item => item.id === product.id)
      let tempProdcut = {
        category: product.category.name,
        id: product.id,
        title: product.title,
        image: product.image,
        price: product.price,
        refPrice: product.price,
        count: 1
      }
      if (preExist !== -1) {
        tempCart[preExist].count = tempCart[preExist].count + 1;
        tempCart[preExist].price = product.price * tempCart[preExist].count;
      } else {
        tempCart?.push(tempProdcut)
      }
      await AsyncStorage.setItem('CART', JSON.stringify(tempCart))
      // alert('Data successfully saved')
    } catch (e) {
      console.log("E _________", e)
      // alert('Failed to save the data to the storage')
    }
    getCartData();
  }

  const clearStorage = async () => {
    try {
      await AsyncStorage.clear();
      // alert('Storage successfully cleared!');
    } catch (e) {
      // alert('Failed to clear the async storage.');
    }
  };

  return (
    <ScrollView>
      <View>
        {!singleProductLoading ? (
          <View>


            <View style={styles.hotCard}>
              <Image
                style={{
                  width: '100%',
                  height: 200,
                  resizeMode: 'center',
                  marginBottom: 30
                }}
                source={{ uri: singleProduct?.image}}
              />
              <View
                style={{
                  position: 'absolute',
                  top: 0,
                  left: 320,
                  right: 0,
                  bottom: 380,
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: 20
                }}>
                <TouchableOpacity
                  onPress={() => {
                    props.navigation.navigate('Cart');
                  }}
                  style={styles.circleCart}
                >
                  <View>
                    <Icon name="shopping-cart" size={20} color="#000000" />
                    {cartItems && <View style={styles.circle1}>
                      <Text style={styles.cartBtnTxt1}>{cartItems?.length}</Text>
                    </View>}
                  </View>
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                style={{
                  position: 'absolute',
                  top: 0,
                  left: 0,
                  right: 330,
                  bottom: 360,
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: 15
                }}
                onPress={() => {
                  props.navigation.navigate('Main');
                }}>
                <View>
                  <View style={styles.circle}>
                    <Icon name="arrow-left" size={15} color="#121212" />
                  </View>
                </View>
              </TouchableOpacity>
            </View>


            <View style={styles.secContainer}>
              <View>
                <View >
                  <View>
                    <Text style={styles.titleText}>{singleProduct?.title}</Text>
                    <Text>{singleProduct?.category}</Text>
                  </View>
                  <View style={{display: 'flex', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between'}}>
                    <View style={styles.star}>
                      {[...Array(~~singleProduct?.rating?.rate)]?.map((_, idx) => {
                        return <View key={idx}>
                          <Icon name="star" size={15} color="#E49B0F" />
                        </View>
                      })}
                    </View>
                    <Text style={styles.priceText}>${singleProduct?.price}</Text>
                  </View>
                </View>
                <View>
                  <Text style={styles.descTitle}>DESCRIPTION</Text>
                  <Text>{truncate(singleProduct?.description, 250)}</Text>
                </View>
              </View>
              <View>
                <View style={styles.cartContainer}>
                  <TouchableOpacity
                    style={styles.cartBtn}
                    onPress={() => saveData(singleProduct)}
                  >
                    <View>
                      <Icon name="shopping-cart" size={20} color="white" />
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => clearStorage()}
                    style={styles.checkBtn}
                  >
                    <View>
                      <Text style={styles.cartBtnTxt}>Checkout</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            </View>


          </View>
        ) : (
          <ContentLoader viewBox="0 0 380 70">
            <Circle cx="30" cy="30" r="30" />
            <Rect x="80" y="17" rx="4" ry="4" width="300" height="13" />
            <Rect x="80" y="40" rx="3" ry="3" width="250" height="10" />
          </ContentLoader>
        )}
        <View />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  iconContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignContent: 'center',
  },
  secContainer: {
    borderTopRightRadius: 45,
    borderTopLeftRadius: 45,
    backgroundColor: 'pink',
    marginTop: -80,
    display: 'flex',
    padding: 30,
    justifyContent: 'space-between',
  },
  hotCard: {
    backgroundColor: 'white',
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    padding: 100
  },
  titleContainer: {
    padding: 20,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  descContainer: {
    paddingHorizontal: 20,

  },
  titleText: {
    fontWeight: '900',
    fontSize: 20,
    color: '#ff69b4',
  },
  descTitle: {
    fontWeight: '900',
    fontSize: 20,
    color: '#000000'
  },
  desc: {
    fontWeight: '500',
    fontSize: 12,
    color: '#000000'
  },
  priceText: {
    fontWeight: '900',
    color: 'red',
    fontSize: 20,
    textAlign: 'right'
  },
  circle: {
    backgroundColor: 'pink',
    width: 30,
    height: 30,
    borderRadius: 15,
    display: 'flex',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  circleCart: {
    marginTop: 15,
    backgroundColor: 'pink',
    width: 40,
    height: 40,
    borderRadius: 20,
    display: 'flex',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  circle1: {
    backgroundColor: '#121212',
    width: 25,
    height: 25,
    borderRadius: 12,
    display: 'flex',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: -15,
    left: -20,
    right: 0,
    bottom: -130,
  },
  cartContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingVertical: 20,
    alignItems: 'center',
  },
  cartBtn: {
    borderRadius: 20,
    flex: 1,
    height: 50,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ff69b4'
  },
  checkBtn: {
    borderRadius: 20,
    flex: 6,
    height: 50,
    marginLeft: 20,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ff69b4'
  },
  cartBtnTxt: {
    color: '#FFFFFF',
    fontWeight: '900',
    fontSize: 20
  },
  cartBtnTxt1: {
    color: '#FFFFFF',
    fontWeight: '900',
    fontSize: 15
  },
  star: {
    display: 'flex',
    flexDirection: 'row',
  }
});

const mapStateToProps = state => {
  return {
    singleProduct: state.products.singleProduct,
    cartItems: state.products.cartItems,
    singleProductLoading: state.products.singleProductLoading,
  };
};

const mapDispatchToProps = dispatch => ({
  getSingleProduct: data => dispatch(getSingleProduct(data)),
  getCartData: () => dispatch(getCartData()),
  fetchSingleProductLoading: data => dispatch(fetchSingleProductLoading(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SingleProduct);
