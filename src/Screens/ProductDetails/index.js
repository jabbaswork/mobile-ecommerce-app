/* eslint-disable react-native/no-inline-styles */
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Image,
} from 'react-native';
import React, { useEffect, useState } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import { getAllCatories, getAllProducts, setCatagory } from '../../Store/Actions';
import { connect } from 'react-redux';
import { truncate } from '../../Utils/functions';

const ProductDetail = props => {
  const { products, navigation } = props;

  const addDefaultSrc = e => {
  };

  return (
    <ScrollView>
      <View style={{ flex: 1 }}>
        <View style={styles.mainContainer}>
          {products && products?.map(item => {
              return (
                <TouchableOpacity
                  onPress={() => {
                    navigation.navigate('SingleProduct', {
                      id: item.id
                    });
                  }}
                  style={styles.newCard}
                  key={item.id}>
                  <View>
                    <View style={styles.imageContainer}>
                      {item.image !== '' ? (
                        <Image
                          style={{
                            width: '100%',
                            height: '100%',
                            resizeMode: 'center'
                          }}
                          source={{ uri: item.image }}
                        />
                      ) : (
                        <Image
                          style={{
                            width: '100%',
                            height: '100%',
                          }}
                          onError={e => addDefaultSrc(e)}
                          source={require(// @ts-ignore
                            '../../Assets/images/coso.webp')}
                        />
                      )}
                      <View
                        style={{
                          position: 'absolute',
                          top: 0,
                          left: 120,
                          right: 0,
                          bottom: 150,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <View style={styles.circle}>
                          {false ? (
                            <Icon name="heart" size={20} color="white" />
                          ) : (
                            <Icon name="heart" size={20} color="red" />
                          )}
                        </View>
                      </View>
                      
                    </View>
                    <View style={styles.nameTextContainer}>
                      
                          <Text style={styles.nameText}>
                            {truncate(item.title, 25)}
                          </Text>
                          <View style={styles.circle}>
                          <Text style={styles.price}>${item.price}</Text>
                          <View style={styles.circle}>{[...Array(~~item.rating.rate)]?.map((a, idx)=> <Icon key={idx} name="star" size={15} color="#E49B0F" />)}</View>
                          </View>
                      </View>
                  </View>
                </TouchableOpacity>
              );
            })}
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  iconContainer: {
    paddingTop: 10,
    paddingHorizontal: 20,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignContent: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  mainContainer: {
    // padding: 20,
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  newCard: {
    marginTop: 18,
    width: '48%',
    height: 270,
    backgroundColor: '#ffffff',
    borderRadius: 20,
    paddingHorizontal: 10
  },
  imageContainer: {
    display: 'flex',
    height: 200,
    padding: 25
  },
  nameTextContainer: {
    paddingHorizontal: 10,
  },
  nameText: {
    color: '#121212',
    fontWeight: 'bold',
    textTransform: 'uppercase',
  },
  price: {
    color: '#ff69b4',
    fontWeight: '900',
    textTransform: 'uppercase',
  },
  circle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignContent: 'center',
    alignItems: 'center',
  },
  hotCard: {
    width: '100%',
    height: 250,
    backgroundColor: 'rgba(38, 37, 37, 0.6)',
    borderRadius: 20,
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  cover: {
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20,
    width: '100%',
    height: 250,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(255, 105, 180, 0.8)',
  },
  coverText: {
    color: '#000000',
    fontWeight: '900',
    textTransform: 'uppercase',
    fontSize: 25,
  },
  menuCircle:{
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: '#FFFFFF',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  cartBtnTxt1: {
    color: '#FFFFFF',
    fontWeight: '900',
    fontSize: 15
  },
  circle1: {
    backgroundColor: '#000000',
    width: 25,
    height: 25,
    borderRadius: 12,
    display: 'flex',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: -1,
    left: -15,
    right: 0,
    bottom: -100,
  },
});

const mapStateToProps = state => {
  return {
    allProducts: state.products.allProducts,
    allCategories: state.products.allCategories,
    selectedCatagory: state.products.selectedCatagory,
    cartItems: state.products.cartItems,
  };
};

const mapDispatchToProps = dispatch => ({
  getAllProducts: () => dispatch(getAllProducts()),
  getAllCatories: () => dispatch(getAllCatories()),
  setCatagory: data => dispatch(setCatagory(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetail);
