/* eslint-disable no-shadow */
/* eslint-disable react-native/no-inline-styles */
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  ScrollView,
} from 'react-native';
import React, { useEffect } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import AntIcon from "react-native-vector-icons/AntDesign";
import { connect } from 'react-redux';
import { getAllCatories, getAllProducts, getCartData } from '../../Store/Actions';
import ProductDetail from '../ProductDetails/index'
import { useState } from 'react';
import { TextInput } from 'react-native-gesture-handler';

const MenuScreen = props => {
  const { allCategories,
    allProducts,
    getAllProducts,
    getAllCatories,
    getCartData,
    navigation,
    cartItems } = props;

  const [products, setProducts] = useState([]);
  const [selected, setSelected] = useState('All');
  const [showFilter, setShowFilter] = useState(false);

  useEffect(() => {
    if (!allCategories) {
      getAllCatories();
    }
    getAllProducts();
    if (!cartItems) {
      getCartData()
    }
  }, []);

  useEffect(() => {
    if (allProducts) {
      setProducts(allProducts)
    }
  }, [allProducts])

  useEffect(() => {
    if (selected == "All") {
      setProducts(allProducts)
    } else {
      let tempArr = allProducts?.filter(item => item.category.toLowerCase() == selected.toLocaleLowerCase());
      setProducts(tempArr)
    }
  }, [selected])

  const searchHandler = (e) => {
    let tempArr = allProducts?.filter(item => item.title.toLowerCase().includes(e.toLocaleLowerCase()));
    setProducts(tempArr)
  }


  return (
    <ScrollView>
      <View style={{ flex: 1, padding: 20 }}>
        <View>
          <View style={styles.iconContainer}>
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate('Home');
              }}>
              <View style={styles.menuCircle}>
                <AntIcon name="appstore1" color="#FFFFFF" size={20} />
              </View>
            </TouchableOpacity>
            <View style={styles.menuCircle}>
              <Icon name="user-circle-o" size={30} color="#FFFFFF" />
            </View>
          </View>
          <View style={{ display: 'flex', flexDirection: 'row', width: '100%', justifyContent: 'space-between', alignItems: 'center' }}>
            <View style={styles.searchContainer}>
              <Icon name="search" size={20} color="#FFFFFF" />
              <TextInput
                style={styles.search}
                // value={query}
                placeholderTextColor={'#FFFFFF'}
                placeholder={'Search...'}
                onChangeText={(e) => searchHandler(e)}
              />
            </View>

            <TouchableOpacity
              style={styles.filContainer}
              onPress={() => { setShowFilter(!showFilter) }}>
              <View >
                <AntIcon name="filter" color="#FFFFFF" size={20} />
              </View>
            </TouchableOpacity>
          </View>

          <ScrollView
            horizontal={true}
            showsHorizontalScrollIndicator={false}
          >
            <View style={styles.catContainer}>
              <TouchableOpacity
                onPress={() => setSelected('All')}>
                <View style={'All' == selected ? styles.newCard : styles.newCardActive}>
                  <Text style={'All' == selected ? styles.catTitle : styles.catTitleActive}>{'ALL'}</Text>
                </View>
              </TouchableOpacity>
              {allCategories &&
                allCategories?.map((item, idx) => {
                  return (
                    <TouchableOpacity
                      key={idx}
                      onPress={() => setSelected(item)}>
                      <View style={item == selected ? styles.newCard : styles.newCardActive}>
                        <Text style={item == selected ? styles.catTitle : styles.catTitleActive}>{item}</Text>
                      </View>
                    </TouchableOpacity>
                  );
                })}
            </View>
          </ScrollView>
        </View>
        <View>
          <ProductDetail
            products={products}
            navigation={navigation}
          />
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  iconContainer: {
    marginTop: 10,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignContent: 'center',
  },
  hotDeals: {
    fontSize: 20,
    textTransform: 'uppercase',
    color: '#262525',
    fontWeight: '900',
  },
  catContainer: {
    display: 'flex',
    flexDirection: 'row',
    marginBottom: 10
  },
  hotCard: {
    marginTop: 18,
    width: '100%',
    height: 150,
    backgroundColor: 'rgba(38, 37, 37, 0.6)',
    borderRadius: 20,
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  newCard: {
    marginRight: 10,
    alignSelf: 'flex-start',
    backgroundColor: '#ff69b4',
    borderRadius: 20,
    paddingHorizontal: 15,
    paddingVertical: 5,
  },
  newCardActive: {
    marginRight: 10,
    alignSelf: 'flex-start',
    backgroundColor: '#FFFFFF',
    borderRadius: 20,
    paddingHorizontal: 15,
    paddingVertical: 5,
    borderWidth: 1,
    borderColor: '#ff69b4'
  },
  title: {
    fontWeight: '400',
    fontSize: 22,
    lineHeight: 27,
    textTransform: 'capitalize',
    color: '#262525',
  },
  catTitle: {
    fontWeight: 'bold',
    fontSize: 20,
    textTransform: 'uppercase',
    color: '#ffffff',
  },
  catTitleActive: {
    fontWeight: 'bold',
    fontSize: 20,
    textTransform: 'uppercase',
    color: '#ff69b4',
  },
  price: {
    fontWeight: 'bold',
    fontSize: 20,
    lineHeight: 32,
    letterSpacing: 0.04,
    textTransform: 'capitalize',
    color: '#62442B',
  },
  circle: {
    backgroundColor: '#62442B',
    width: 38,
    height: 38,
    borderRadius: 20,
    display: 'flex',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  imageContainer: {
    display: 'flex',
    height: 240,
  },
  image: {
    width: '100%',
    height: '100%',
  },
  textContainer1: {
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: 20,
  },
  discountContainer: {
    marginTop: 30,
    marginRight: 20,
  },
  discountContainer2: {
    display: 'flex',
    flexDirection: 'row',
  },
  discount: {
    fontWeight: '700',
    fontSize: 14,
    textTransform: 'uppercase',
    color: '#FFFFFF',
  },
  discount2: {
    fontWeight: '500',
    fontSize: 14,
    textTransform: 'uppercase',
    color: '#FFFFFF',
  },
  recentCard: {
    width: '100%',
    height: 108,
    backgroundColor: '#ff69b4',
    borderRadius: 20,
    marginTop: 18,
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  recentCard2: {
    display: 'flex',
    flexDirection: 'row',
  },
  recentCard3: {
    width: 69,
    height: 77,
    backgroundColor: 'rgba(255, 255, 255, 0.4)',
    borderRadius: 18,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },
  recentCard4: {
    marginLeft: 10,
  },
  detailsContainer: {
    backgroundColor: '#62442B',
    width: 50,
    height: 30,
    borderRadius: 10,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 25,
  },
  detailsText: {
    color: '#ffff',
    fontWeight: 'bold',
  },
  nameTextContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 15,
  },
  nameText: {
    color: '#FFFFFF',
    fontWeight: '900',
    fontSize: 20,
    textTransform: 'uppercase',
  },
  menuCircle: {
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: '#ff69b4',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  searchContainer: {
    width: '83%',
    marginVertical: 20,
    backgroundColor: '#ff69b4',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 15,
    paddingHorizontal: 15
  },
  searchContainerFilt: {
    width: '49%',
    marginVertical: 20,
    backgroundColor: '#ff69b4',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 15,
    paddingHorizontal: 15
  },
  search: {
    backgroundColor: '#ff69b4',
    marginLeft: 10,
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
  searchFil: {
    backgroundColor: '#ff69b4',
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
  filContainer: {
    backgroundColor: '#ff69b4',
    height: 48,
    width: '15%',
    borderRadius: 12,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dropdown: {
    position: 'absolute',
    backgroundColor: '#fff',
    top: 120,
    width: '100%',
    height: 200,
    zIndex: 2,
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    padding: 16
  },
  navigationContainer: {
    backgroundColor: "#ecf0f1"
  },
  paragraph: {
    padding: 16,
    fontSize: 15,
    textAlign: "center"
  }

});

const mapStateToProps = state => {
  return {
    allProducts: state.products.allProducts,
    allCategories: state.products.allCategories,
    cartItems: state.products.cartItems,
  };
};

const mapDispatchToProps = dispatch => ({
  getAllProducts: () => dispatch(getAllProducts()),
  getAllCatories: () => dispatch(getAllCatories()),
  getCartData: () => dispatch(getCartData()),
});

export default connect(mapStateToProps, mapDispatchToProps)(MenuScreen);
