import {
  GET_ALL_CATEGORIES,
  GET_ALL_PRODUCTS,
  GET_CART_ITEMS,
  GET_SINGAL_PRODUCT,
  GET_SINGAL_PRODUCT_LOADING,
  GET_TOTAL_PRICE,
  SELCTED_CATAGORY,
} from '../Types/index';

const initialState = {
  allProducts: false,
  allProductsLoading: false,
  allCategories: false,
  allCategoriesLoading: false,
  singleProduct: false,
  singleProductLoading: false,
  selectedCatagory: false,
  cartItems: false,
  totalPrice: false,
};
/**
 * @param {{ type: any; data: any; }} action
 */
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_PRODUCTS:
      return {
        ...state,
        allProducts: action.data,
      };
    case GET_ALL_CATEGORIES:
      return {
        ...state,
        allCategories: action.data,
      };
    case GET_SINGAL_PRODUCT:
      return {
        ...state,
        singleProduct: action.data,
      };
    case GET_SINGAL_PRODUCT_LOADING:
      return {
        ...state,
        singleProductLoading: action.data,
      };
    case SELCTED_CATAGORY:
      return {
        ...state,
        selectedCatagory: action.data,
      };
      case GET_CART_ITEMS:
        return {
          ...state,
          cartItems: action.data,
        };
    case GET_TOTAL_PRICE:
      return {
        ...state,
        totalPrice: action.data,
      };
    default:
      return state;
  }
};

export default reducer;
