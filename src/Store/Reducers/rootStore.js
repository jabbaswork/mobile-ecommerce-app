import {combineReducers} from 'redux';
import products from './index';

export default combineReducers({
  products,
});
