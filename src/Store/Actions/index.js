import {
  GET_ALL_PRODUCTS,
  GET_ALL_CATEGORIES,
  GET_SINGAL_PRODUCT,
  SELCTED_CATAGORY,
  GET_CART_ITEMS,
  GET_TOTAL_PRICE,
  GET_SINGAL_PRODUCT_LOADING,
} from '../Types/index';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

const apiUrl = 'https://fakestoreapi.com/';

export const fetchProducts = data => {
  return {
    type: GET_ALL_PRODUCTS,
    data,
  };
};

export const fetchCategories = data => {
  return {
    type: GET_ALL_CATEGORIES,
    data,
  };
};

export const fetchSingleProduct = data => {
  return {
    type: GET_SINGAL_PRODUCT,
    data,
  };
};

export const fetchSingleProductLoading = data => {
  return {
    type: GET_SINGAL_PRODUCT_LOADING,
    data,
  };
};
export const setCatagory = data => {
  return {
    type: SELCTED_CATAGORY,
    data,
  };
};

export const getCartItems = data => {
  return {
    type: GET_CART_ITEMS,
    data,
  };
};

export const getTotal = data => {
  return {
    type: GET_TOTAL_PRICE,
    data,
  };
};

//LOCAL_STORAGE FUNCTIONS ONLY

export const getCartData = () => {
  return async (dispatch) => {
    try {
      let tempCart = JSON.parse(await AsyncStorage.getItem('CART') || "[]");
      if (tempCart !== null) {
        dispatch((getCartItems(tempCart)))
        if(tempCart?.length){
          let total = tempCart.map(item => item.price).reduce((prev, next) => prev + next);
          dispatch((getTotal(total)))
        } 
        
      }
    } catch (error) {
      console.log("error____________", error)
      // alert('Failed to fetch the CART ITEMS from storage');
    }
  };

};

//HERE API FUNCTIONS ONLY
export const getAllProducts = () => {
  try {
    return async dispatch => {
      const configurationObject = {
        method: 'get',
        url: `https://fakestoreapi.com/products`,
      };
      const response = await axios(configurationObject);

      dispatch(fetchProducts(response.data))
    }
  } catch (error) {
    console.log("error____________", error)
  };
};

export const getAllCatories = () => {
  try {
    return async dispatch => {
      const configurationObject = {
        method: 'get',
        url: `https://fakestoreapi.com/products/categories`,
      };
      const response = await axios(configurationObject);

      dispatch(fetchCategories(response.data))
    }
  } catch (error) {
    console.log("error____________", error)
  };
};

export const getSingleProduct = id => {
  try {
    return async dispatch => {
      const configurationObject = {
        method: 'get',
        url: `https://fakestoreapi.com/products/${id}`,
      };
      const response = await axios(configurationObject);

      dispatch(fetchSingleProduct(response.data))
    }
  } catch (error) {
    console.log("error____________", error)
  };
};
