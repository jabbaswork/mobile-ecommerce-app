export const truncate = function(str, length=16, ending='...') {
    if (str?.length > length) {
      return str.substring(0, length - ending.length) + ending;
    } else {
      return str;
    }
  };
