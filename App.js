import React, { useEffect } from 'react';
import HomeScreen from './src/Screens/HomeScreen';
import MenuScreen from './src/Screens/Menu';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {View, Text} from 'react-native';
import ProductDetails from './src/Screens/ProductDetails';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import ChatScreen from './src/Screens/Chat';
import Profile from './src/Screens/Profile';
import SingleProduct from './src/Screens/ProductDetails/SingleProduct';
import CartScreen from './src/Screens/Cart/index';
import { connect } from 'react-redux';
import { getCartData } from './src/Store/Actions';

const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

function HomeStack() {
  return (
    <Stack.Navigator
      initialRouteName="Home"
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="Main" component={MenuScreen} />
      <Stack.Screen name="ProductDetail" component={ProductDetails} />
      <Stack.Screen name="SingleProduct" component={SingleProduct} />
    </Stack.Navigator>
  );
}

const App = (props) => {

  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({route}) => ({
          headerShown: false,
          tabBarIcon: ({focused, color, size}) => {
            let iconName;

            if (route.name === 'Home') {
              iconName = focused ? 'home' : 'home-outline';
            } else if (route.name === 'Chat') {
              iconName = focused
                ? 'chatbox-ellipses'
                : 'chatbox-ellipses-outline';
            } else if (route.name === 'Cart') {
              iconName = focused ? 'cart' : 'cart-outline';
            } else if (route.name === 'Profile') {
              iconName = focused ? 'person-circle' : 'person-circle-outline';
            }
            // You can return any component that you like here!
            return <Ionicons name={iconName} size={size} color={color} />;
          },
          tabBarActiveTintColor: '#62442B',
          tabBarInactiveTintColor: 'gray',
        })}>
        <Tab.Screen name="Home" component={HomeStack}/>
        <Tab.Screen name="Chat" component={ChatScreen} />
        <Tab.Screen name="Cart" component={CartScreen} />
        <Tab.Screen name="Profile" component={Profile} />
      </Tab.Navigator>
    </NavigationContainer>
  );
};


const mapStateToProps = state => {
  return {
    cartItems: state.products.cartItems,
  };
};

const mapDispatchToProps = dispatch => ({
  getCartData: () => dispatch(getCartData()),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
